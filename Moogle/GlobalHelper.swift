//
//  GlobalHelper.swift
//  Moogle
//
//  Created by David Velarde on 5/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

enum ErrorType{
    case connection
}

func showError(_ error:ErrorType){
    
    var message = ""
    
    switch error {
    case .connection:
        message = "There seems to be an error on your connection.\nPlease try again later"
    }
    
    let controller = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
    controller.addAction(action)
    if let delegate = UIApplication.shared.delegate as? AppDelegate {
        delegate.window?.rootViewController?.present(controller, animated: true, completion: nil)
    }
}
