//
//  SearchViewController.swift
//  Moogle
//
//  Created by David Velarde on 5/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import Moya
import DZNEmptyDataSet

class SearchViewController: MoogleViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tvResults: UITableView!
    
    var arrayResults = [Movie]()
    
    var isLoading = false
    var isSearching = false
    var moreMoviesAvailable = true
    var currentQuery = ""
    var currentPage = 1
    var currentRequest : Cancellable?
    
    
    override func viewDidLoad() {
        icon = "🔎"
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tvResults.estimatedRowHeight = 80
        tvResults.rowHeight = UITableViewAutomaticDimension
        
        tvResults.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func loadMoreResults(){
        search(forQuery: self.currentQuery)
    }
    
    func search(forQuery query:String) {
        if isSearching == false{
            isSearching = true
        }
        
        if currentQuery != query {
            currentPage = 1
            moreMoviesAvailable = true
            arrayResults.removeAll()
        }
        else{
            if !moreMoviesAvailable {
                tvResults.reloadData()
                return
            }
        }
        if currentRequest != nil {
            if !currentRequest!.isCancelled {
                currentRequest!.cancel()
                isLoading = false
            }
        }
        tvResults.reloadData()
        
        self.currentQuery = query
        if !isLoading{
            isLoading = true
            
            currentRequest = traktProvider.request(TraktService.search(query: query, page: currentPage)) { result in
                self.isLoading = false
                switch result {
                case let .success(moyaResponse):
                    if self.currentPage == 1 {
                        self.arrayResults.removeAll()
                    }
                    if let newData = try? moyaResponse.mapArray(Movie.self), newData.count > 0 {
                        self.addNewMoviesToTable(newData: newData)
                        self.currentPage+=1
                    }
                    else{
                        self.moreMoviesAvailable = false
                        self.tvResults.reloadData()
                    }
                case .failure:
                    print("")
                    //showError(.connection)
                }
            }
        }

    }
    func addNewMoviesToTable(newData:[Movie]){
        let nonRepeatedMovies = newData.filter { (movieToAdd) -> Bool in
            
            return !(arrayResults.contains(where: { movie -> Bool in
                        return (movie.tmdbId == movieToAdd.tmdbId)
                    }))
            
        }
        arrayResults.append(contentsOf: nonRepeatedMovies)
        tvResults.reloadData()
    }
}

extension SearchViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return arrayResults.count + (moreMoviesAvailable ? 1 : 0)
        }
        else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if moreMoviesAvailable && indexPath.row == arrayResults.count {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell") {
                let activity : UIActivityIndicatorView = cell.viewWithTag(1) as! UIActivityIndicatorView
                activity.isHidden = false
                activity.startAnimating()
                return cell
            }
            
        }
        else{
            let currentMovie = arrayResults[indexPath.row]
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell") as? MovieTableViewCell {
                
                cell.prepare(forMovie: currentMovie)
                cell.currentIndexPath = indexPath
                return cell
            }
        }
        return UITableViewCell()
    }
}

extension SearchViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText != "" && searchText != "\n" {
            self.search(forQuery: searchText)
        }
        else{
            isSearching = false
            arrayResults.removeAll()
            tvResults.reloadData()
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension SearchViewController : UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        searchBar.resignFirstResponder()
        
    }
}

extension SearchViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if arrayResults.count > 0 && indexPath.row == arrayResults.count {

            loadMoreResults()

        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMovie = arrayResults[indexPath.row]
        performSegue(withIdentifier: "goToDetail", sender: self)
    }
}

extension SearchViewController : DZNEmptyDataSetSource {
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        var message = ""
        
        if isSearching {
            if isLoading {
                message = "Searching..."
            }
            else{
                message = "No Results"
            }
        }
        else{
            message = "Start typing a movie name"
        }
        
        
        return NSAttributedString(string: message, attributes:nil)
    }
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return -100
    }
    
}
