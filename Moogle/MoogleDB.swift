//
//  MoogleDB.swift
//  Moogle
//
//  Created by David Velarde on 5/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import RealmSwift

class MoogleDB: NSObject {

    static let shared = MoogleDB()
    
    let realm = try! Realm()
    
    func getURL(forTMDBId tmdbId:Int)->URL?{
        if let movie = realm.object(ofType: MovieDB.self, forPrimaryKey: tmdbId) {
            if let url = URL(string: movie.posterURL) {
                return url
            }
        }
        return nil
    }

    func addOrUpdateMovie(tmdbId:Int, posterUrl:String) {
        
        if let movie = realm.object(ofType: MovieDB.self, forPrimaryKey: tmdbId) {
            try! realm.write {
                movie.posterURL = posterUrl
            }
        }
        else{
            
            let movie = MovieDB()
            
            movie.tmdbId = tmdbId
            movie.posterURL = posterUrl
            
            try! realm.write {
                realm.add(movie)
            }
        }
    }
    
}
