//
//  Movie.swift
//  Moogle
//
//  Created by David Velarde on 5/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import Gloss
import Nuke
class Movie: Decodable {
    
    var title : String!
    var year : Int!
    var tmdbId : Int!
    var overview : String!
    var posterURL : URL?
    var gotURL : ((URL)->Void)?
    
    required init?(json:JSON){
        
        var source = json
        
        if let movie : JSON = "movie" <~~ json {
            source = movie
        }
        guard
        
            let title : String = "title" <~~ source,
            let year : Int = "year" <~~ source,
            let tmdbId : Int = "ids.tmdb" <~~ source,
            let overview : String = "overview" <~~ source
            else{ return nil }
        
        self.title = title
        self.year = year
        self.tmdbId = tmdbId
        self.overview = overview
        
        self.fetchPosterURL()
    }
    
    
    
    func fetchPosterURL(){
        
        if let url = MoogleDB.shared.getURL(forTMDBId: self.tmdbId) {
            self.posterURL = url
            return
        }
        
        tmdbProvider.request(TMDBService.getImages(movieId: self.tmdbId)) { result in
            switch result {
            case let .success(moyaResponse):
                do{
                if let imagesData = try moyaResponse.mapJSON() as? JSON{
                    if let posterArray : [JSON] = "posters" <~~ imagesData {
                        if let firstPoster = posterArray.first {
                            if let posterEndpoint : String = "file_path" <~~ firstPoster {
                                let sURL = "https://image.tmdb.org/t/p/w500\(posterEndpoint)"
                                print("Creating URL for: \(sURL)")
                                guard let url = URL(string: sURL) else {
                                    print("Failed URL creation for \(sURL)")
                                    return
                                }
                                MoogleDB.shared.addOrUpdateMovie(tmdbId: self.tmdbId, posterUrl: url.absoluteString)
                                let preheater = Preheater(manager: Manager.shared)
                                let requests = [Request(url: url)]
                                preheater.startPreheating(with: requests)
                                self.posterURL = url
                                if let gotURL = self.gotURL {
                                    gotURL(url)
                                }
                            }
                        }
                    }
                }
                }
                catch(let error){
                    print(error.localizedDescription)
                }
            case let .failure(error):
                print(error.errorDescription!)
            }
            
        }
    }
    
}
